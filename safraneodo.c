#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <endian.h>
#include <errno.h>
#include <string.h>
#include <stdbool.h>

// EEPROM works in 16bit org.
#define EEPROM_SIZE_WORDS 0x80
#define EEPROM_SIZE_BYTES EEPROM_SIZE_WORDS * 2

// Const. values
#define STRING_BUFFER_SIZE 256
#define MAX_MILLAGE_KM 777187
#define MIN_MILLAGE_KM 0

// Read millage
int readodo(uint16_t *eeprom)
{
    uint16_t rawread, accurateread;
    int millage, index;

    // Read millage accurate to 32km (raw)
    rawread = le16toh(eeprom[0x6E]);
    millage = (int)(0xFFFF - rawread) * 32;

    // Read millage accurate to 2km (accurate)
    accurateread = htole16(le16toh(eeprom[0x6E]) + 1);
    for (index = 0x7D; index >= 0x6E; index -= 0x01)
    {
        if (index == 0x78 || index == 0x70)
            continue;
        if (eeprom[index] == accurateread)
            millage -= 2;
        else if (eeprom[index] == rawread)
            ;
        else
            fprintf(stderr, "Invalid data at 0x%02X, skipping.\n", index * 2);
    }

    return millage;
}

// Update odo
void changeodo(uint16_t *eeprom, int millage)
{
    uint16_t rawmillage, accuratemillage;
    int index, tolose;

    // Calculate raw millage
    millage += 32;
    rawmillage = 0xFFFF - (uint16_t)(millage / 32);
    rawmillage = htole16(rawmillage);

    // Save raw millage to EEPROM
    eeprom[0x6E] = rawmillage;

    // Calculate accurate millage
    accuratemillage = htole16(le16toh(rawmillage) + 1);
    tolose = 32 - (millage - (millage / 32 * 32));

    // Save accurate millage
    for (index = 0x7D; index >= 0x6E; index -= 0x01)
    {
        if (index == 0x78 || index == 0x70)
        {
            continue;
        }
        else
        {
            if (tolose > 0)
            {
                tolose -= 2;
                eeprom[index] = accuratemillage;
            }
            else
            {
                eeprom[index] = rawmillage;
            }
        }
    }
}

int main(int argc, char **argv)
{
    // Variables like in C89
    FILE *infile, *outfile;
    uint16_t eeprom[EEPROM_SIZE_WORDS]; // EEPROM works in 16 bit org.
    long newmillage;
    int size, index, currentodo;
    char stringbuffer[STRING_BUFFER_SIZE];
    char *charptrtemp;
    bool updateodo;

    // Check arguments
    if (argc != 2)
    {
        // Print usage and DIE
        fprintf(stderr, "Usage: %s <EEPROM dump>\n", argv[0]);
        fprintf(stderr, "(c) Adrian Grzeca 2024\n\n");
        fprintf(stderr, "Usage only for educational purpose!!\n");
        fprintf(stderr, "Do not use this program to lower millage on your car before selling!!\n");
        exit(-1);
    }

    // Try open file
    fprintf(stderr, "Trying to open %s\n", argv[1]);
    infile = fopen(argv[1], "r");
    if (infile == NULL)
    {
        fprintf(stderr, "Cannot open file!\n");
        exit(-1);
    }

    // Check file size
    fseek(infile, 0, SEEK_END);
    if (ftell(infile) != EEPROM_SIZE_BYTES)
    {
        fprintf(stderr, "Invalid file size! Excepted %dB got %ldB.\n", EEPROM_SIZE_BYTES, ftell(infile));
        exit(-1);
    }
    fseek(infile, 0, SEEK_SET);

    // Read file to buffer
    if (fread(eeprom, 2, EEPROM_SIZE_WORDS, infile) != EEPROM_SIZE_WORDS)
    {
        fprintf(stderr, "Reading fail!\n");
        exit(-1);
    }

    // Read odo
    fprintf(stderr, "Reading current millage..\n");
    currentodo = readodo(eeprom);
    fprintf(stderr, "Current millage: %dkm\n", currentodo);

    // Do you want to change millage?
    for (;;)
    {
        // Read input
        fprintf(stderr, "Do you want do \"update\" millage (yes or no): ");

        // Input
        if (fgets(stringbuffer, STRING_BUFFER_SIZE, stdin) == NULL)
        {
            fprintf(stderr, "Reading from stdin failed!\n");
            exit(-1);
        }

        // Success
        if (strncmp(stringbuffer, "yes\n", STRING_BUFFER_SIZE) == 0)
        {
            updateodo = true;
            break;
        }
        else if (strncmp(stringbuffer, "no\n", STRING_BUFFER_SIZE) == 0)
        {
            updateodo = false;
            break;
        }
        else
        {
            fprintf(stderr, "Enter \"yes\" or \"no\"!\n");
        }
    }

    // Update odo path
    if (updateodo)
    {
        // Input new odo
        for (;;)
        {
            // Read input
            fprintf(stderr, "Enter desired millage in km: ");

            // Input
            if (fgets(stringbuffer, STRING_BUFFER_SIZE, stdin) == NULL)
            {
                fprintf(stderr, "Reading from stdin failed!\n");
                exit(-1);
            }

            // Success
            newmillage = strtol(stringbuffer, &charptrtemp, 10);
            if (charptrtemp == stringbuffer)
            {
                fprintf(stderr, "Enter valid number!\n");
            }
            else if (*charptrtemp != '\n')
            {
                fprintf(stderr, "Enter valid number! Without any additional text.\n");
            }
            else
            {
                if (newmillage > MAX_MILLAGE_KM || newmillage < MIN_MILLAGE_KM)
                {
                    fprintf(stderr, "Enter valid number! Between %dkm and %d.\n", MIN_MILLAGE_KM, MAX_MILLAGE_KM);
                }
                else
                {
                    break;
                }
            }
        }

        // Opening output file
        snprintf(stringbuffer, STRING_BUFFER_SIZE, "%s_updated_%ld_km.bin", argv[1], newmillage);
        fprintf(stderr, "Output EEPROM dump path %s\n", stringbuffer);

        // Try open file
        outfile = fopen(stringbuffer, "r");
        if (outfile != NULL)
        {
            fprintf(stderr, "Output file already exists! Rename it. Exiting..\n");
            exit(-1);
        }

        // Open file to write
        outfile = fopen(stringbuffer, "w+");
        if (outfile == NULL)
        {
            fprintf(stderr, "Cannot create output file!\n");
            exit(-1);
        }

        // Update EEPROM
        changeodo(eeprom, (int)newmillage);
        currentodo = readodo(eeprom);
        fprintf(stderr, "New millage: %dkm\n", currentodo);

        // Write EEPROM to file
        if (fwrite(eeprom, 2, EEPROM_SIZE_WORDS, outfile) != EEPROM_SIZE_WORDS)
        {
            fprintf(stderr, "Writing fail!\n");
            exit(-1);
        }

        // Close file
        fclose(outfile);
    }

    return 0;
}